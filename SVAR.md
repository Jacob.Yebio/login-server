HUSK Å GJØRE GIT PUBLIC FØR DU LEVERER  


a brief overview of your design considerations from Part A,

The most obvious problem is the terrible structure of the code. This makes it very hard to understand how everything is 
connected, and therefore very hard to change or implement anything at all. 
It also decreases the chance of security flaws being noticed. Instead of having everything in app.py one should therefore
split it in to several python files. There does exist some extra files, but these are unused and does therefore not 
help at all. On the contrary these files just make the project more unsafe as it increases the options attackers have.
(possibility for backdoor injection)

A couple of security flaws in the code I noticed:

There is no password check when logging in, only a username check.

The bar for searching through messages is vulnerable to SQL injections.
The send message bar is vulnerable to cross-site scripting.

The best solution to this would be to add prepared statements, which I have done in most places. 
And it would have taken me less time if the code was better structured. 

Users is stored in an array in the program code. And neither the username nor the passwords have been encrypted.
A better solution would be to store them in a database. So that even if someone gets hold of the source code, 
the users, passwords and information sent between them would not be found.

No way to log out. This is obviously not secure as no user is going to keep control over their screen at all time.
Meaning anyone close by who gets hold of their device can access it. Do to this I added a logout button.

There is no way to add more users, though this is not optimal it does not make the site less secure. Unless it makes
people share their username and password with others. 
If you forget your password there is no way of getting it back. This is good for security, but not user experience.

The site would crash after logging in, out and then trying to log in again.

Do to there not being any cross-site request forgery token checks, one could be tricked by opening a link you have 
received from someone else. 
There are also cross-site request forgery

the features of your application,

I tried to separate app.py into different files. This did not go very well, so I decided to come back to that later.
Which never happened do to little time and a lot of frustration. 
The plan was to move users to a database and structure the code in the end. I came close 
with the database, but the end came too soon. 

I created the logout button (so that users could log out) and was happy that it worked. The focus then 
went to making sure you actually needed a password to log in. Followed by focusing on making it safe from SQL injections.


instructions on how to test/demo it,

You run it the same way you run the sourcecode we were given.

You log in with users from the code in app.py. 
hei-123
alice-password123
bob-bananas

You send messages the same way as the sourcecode we were given.
You log out by pressing log out.


Threat model

As stated earlier there is alot an attacker could do seeing how it is in danger of SQL injections, cross-site scripting
and cross-site request forgery. 
Do to the database not being encrypted the confidentiality would be breached if someone get hold of it.
If someone does get the sourcecode they cold easily change the username or passwords which means that the integrity 
is under scrutiny.


By using prepared statements one can protect against SQL injections. 





